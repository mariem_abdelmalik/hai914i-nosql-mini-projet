package rdfQueryEngine.query;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;
import org.eclipse.rdf4j.query.algebra.StatementPattern;
import org.eclipse.rdf4j.query.algebra.helpers.StatementPatternCollector;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.eclipse.rdf4j.query.parser.sparql.SPARQLParser;

import rdfQueryEngine.dictionary.Dictionary;
import rdfQueryEngine.index.Index;

public class Queries {

	public String queryFile;
	
	private Dictionary dictionary;
	
	private Index index;
	
	private Map<String,List<String>> queriesResults;
	
	private Map<ParsedQuery,Integer> parsedQueriesNbResponse;
	
	private Map<ParsedQuery,Integer> parsedQueriesNbRepetitions;

	private int nbQuesiesWithZeroResponse;
	
	private List<Integer> nbQuesiesWithSameNbPatronsFromOneToTen;
	
	private List<Integer> nbRepetitionsInQueries;

	
	public Queries() {
		
		this.queriesResults = new HashMap<String,List<String>>();
		
		this.nbQuesiesWithZeroResponse = 0;
		
		this.nbQuesiesWithSameNbPatronsFromOneToTen = new ArrayList<Integer>();
		
		this.nbRepetitionsInQueries = new ArrayList<Integer>();
		
		this.parsedQueriesNbResponse = new HashMap<ParsedQuery,Integer>();
		
		this.parsedQueriesNbRepetitions = new HashMap<ParsedQuery,Integer>();


	}
	
	//public init
	

	public  void parseQueries(String baseURI) throws FileNotFoundException, IOException {
		
		ArrayList<String> resultat = new ArrayList<String>();
 
		try (Stream<String> lineStream = Files.lines(Paths.get(queryFile))) {
			
			SPARQLParser sparqlParser = new SPARQLParser();
			
			Iterator<String> lineIterator = lineStream.iterator();
			
			StringBuilder queryString = new StringBuilder();

			while (lineIterator.hasNext())
		
			{
				String line = lineIterator.next();
				queryString.append(line);

				if (line.trim().endsWith("}")) {

					ParsedQuery query = sparqlParser.parseQuery(queryString.toString(), baseURI);		

					resultat =  this.evaluateQuery(query);
					
					parsedQueriesNbResponse.put(query,resultat.size());
					
					this.queriesResults.put(query.toString(),resultat);

					queryString.setLength(0); 
					
				}
			}
		}
	}
	
	public ArrayList<String> evaluateQuery(ParsedQuery parserdQuery) {
		
		ArrayList<String> resultat = new ArrayList<String>();

		List<StatementPattern> patterns = StatementPatternCollector.process(parserdQuery.getTupleExpr());
		
		ArrayList<Integer> elements = new ArrayList<Integer>();
		
		Integer object_id = this.dictionary.getId(patterns.get(0).getObjectVar().getValue().stringValue());;

		Integer predicat_id =  this.dictionary.getId(patterns.get(0).getPredicateVar().getValue().stringValue()); ;
		
		elements = index.getElements(object_id, predicat_id);

		ArrayList<Integer> l = new ArrayList<Integer>();

		for (int i=1;i<patterns.size();i++) {
			
			object_id = this.dictionary.getId(patterns.get(i).getObjectVar().getValue().stringValue());

			predicat_id = this.dictionary.getId(patterns.get(i).getPredicateVar().getValue().stringValue()) ;
			
			l = this.index.getElements(object_id, predicat_id);

			elements.retainAll(l);
			
			if (elements.isEmpty()) {	
				
				this.nbQuesiesWithZeroResponse++;
				
				return resultat;
				
			}
			
		}

		String s;

		for (Integer e : elements) {
				
			s=dictionary.getValue(e);
				
			resultat.add(s);
							
		}
		
		return resultat;
		
	}
	
	public boolean AreEquals(ParsedQuery parsedQuery1, ParsedQuery parsedQuery2) {
		
		List<StatementPattern> patterns1 = StatementPatternCollector.process(parsedQuery1.getTupleExpr());

		List<StatementPattern> patterns2 = StatementPatternCollector.process(parsedQuery2.getTupleExpr());

		return patterns1.equals(patterns2);
		
	}
	
	public void initNbRepetitionsInQueries() {
		
	this.nbRepetitionsInQueries =  new ArrayList<Integer>(Collections.nCopies(10, 0));
		
	int nbRepetions = 0;
	
	int j = 0;
	
	List<ParsedQuery> prasedQueries = new ArrayList<ParsedQuery>(this.parsedQueriesNbResponse.keySet());
	
	while (prasedQueries.size()>0)
	{
		
		this.parsedQueriesNbRepetitions.put(prasedQueries.get(0),this.getNbOccurenceAndRemoveThem(prasedQueries, prasedQueries.get(0)));
		
	}
		for (ParsedQuery parsedQuery : this.parsedQueriesNbRepetitions.keySet() ) {
			
				nbRepetions = this.parsedQueriesNbRepetitions.get(parsedQuery);
				
				j = this.nbRepetitionsInQueries.get(nbRepetions-1);
						
				j++;
				
				this.nbRepetitionsInQueries.set(nbRepetions-1,j);
				
			}
		
	}
	
	public int getNbOccurenceAndRemoveThem(List<ParsedQuery> parsedQueries,ParsedQuery parsedQuery){
		
		int nbOccurence = -1;
		
		List<Integer> l = new ArrayList<>();
		
		for(int i = 0; i<parsedQueries.size();i++) {
			
			if (AreEquals(parsedQueries.get(i),parsedQuery)) {
				
				nbOccurence++;
				
				l.add(i);
				
			}
		}
		
		for(int x = l.size() - 1; x > 0; x--)
		{
			parsedQueries.remove(x);
		}
	
		
		return nbOccurence;
			
	}
	/*
	public void initParsedQueriesNbRepetitions(){
		
		int nbRepetions;
		
		List<ParsedQuery> parsedQueries =  new ArrayList<ParsedQuery>();
		
		parsedQueries = new ArrayList<ParsedQuery>(this.parsedQueriesNbResponse.keySet());
		
		for (int i = 0; i < parsedQueries.size(); i++) {
			
			getNbOccurence(parsedQueries,parsedQueries.get(i));
			
			//this.parsedQueriesNbRepetitions.put(parsedQueries.get(i), nbRepetions);
			
		}
		
	}*/

	public void initNbQuesiesWithSameNbPatronsFromOneToTen() {
		
		this.nbQuesiesWithSameNbPatronsFromOneToTen =  new ArrayList<Integer>(Collections.nCopies(10, 0));
		
		int i;
		
		for (ParsedQuery parsedQuery : this.parsedQueriesNbResponse.keySet()) {
			
			List<StatementPattern> patterns = StatementPatternCollector.process(parsedQuery.getTupleExpr());

			i = this.nbQuesiesWithSameNbPatronsFromOneToTen.get(patterns.size()-1);
			
			i=i+1;
						
			nbQuesiesWithSameNbPatronsFromOneToTen.set(patterns.size() -1, i);
			
		}
		
	}
	
	public int getNbQuesiesWithZeroResponse() {
		return nbQuesiesWithZeroResponse;
	}

	public void setNbQuesiesWithZeroResponse(int nbQuesiesWithZeroResponse) {
		this.nbQuesiesWithZeroResponse = nbQuesiesWithZeroResponse;
	}

	public String getQueryFile() {
		return queryFile;
	}

	public void setQueryFile(String queryFile) {
		this.queryFile = queryFile;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	public Index getIndex() {
		return index;
	}

	public void setIndex(Index index) {
		this.index = index;
	}

	public Map<String, List<String>> getQueriesResults() {
		return queriesResults;
	}

	public void setQueriesResults(Map<String, List<String>> queriesResults) {
		this.queriesResults = queriesResults;
	}

	

	public List<Integer> getNbQuesiesWithSameNbPatronsFromOneToTen() {
		return nbQuesiesWithSameNbPatronsFromOneToTen;
	}


	public void setNbQuesiesWithSameNbPatronsFromOneToTen(List<Integer> nbQuesiesWithSameNbPatronsFromOneToTen) {
		this.nbQuesiesWithSameNbPatronsFromOneToTen = nbQuesiesWithSameNbPatronsFromOneToTen;
	}

	public Map<ParsedQuery, Integer> getParsedQueriesNbResponse() {
		return parsedQueriesNbResponse;
	}


	public void setParsedQueriesNbResponse(Map<ParsedQuery, Integer> parsedQueriesNbResponse) {
		this.parsedQueriesNbResponse = parsedQueriesNbResponse;
	}


	public Map<ParsedQuery, Integer> getParsedQueriesNbRepetitions() {
		return parsedQueriesNbRepetitions;
	}


	public void setParsedQueriesNbRepetitions(Map<ParsedQuery, Integer> parsedQueriesNbRepetitions) {
		this.parsedQueriesNbRepetitions = parsedQueriesNbRepetitions;
	}

	public List<Integer> getNbRepetitionsInQueries() {
		return nbRepetitionsInQueries;
	}

	public void setNbRepetitionsInQueries(List<Integer> nbRepetitionsInQueries) {
		this.nbRepetitionsInQueries = nbRepetitionsInQueries;
	}



	
	
}
