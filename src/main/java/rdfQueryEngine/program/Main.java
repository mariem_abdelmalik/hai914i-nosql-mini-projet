package rdfQueryEngine.program;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.stream.Stream;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.BasicConfigurator;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.eclipse.rdf4j.query.parser.sparql.SPARQLParser;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;

import rdfQueryEngine.data.Data;
import rdfQueryEngine.data.MainRDFHandler;
import rdfQueryEngine.dictionary.Dictionary;
import rdfQueryEngine.index.Index;
import rdfQueryEngine.query.Queries;

public class Main {
	
	static final String baseURI = null;
	
	static String dataFile = "data/data.nt";
	
	static String queryFile = "data/querys.queryset";
	
	static String outputFile;
    
	static Data data;
	
	static Index index;
	
	static Dictionary dictionary;
	
	static Queries queries;

	static StopWatch AllProgrammeTime = new StopWatch( );
		
	public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
		// TODO Auto-generated method stub
	
		
		//AllProgrammeTime.start();
		
		testForTwoInstances();
		
		//AllProgrammeTime.stop();

		//System.out.println(AllProgrammeTime);
	
	}
	
	public static void init(String dataFile, String queryFile) throws FileNotFoundException, IOException {
		
		dictionary = new Dictionary();
		
		index = new Index();
		
		data = new Data();
		
		queries = new Queries();
		
		data.setDataFile(dataFile);
		
		data.setDictionnary(dictionary);
		
		data.setIndex(index);
		
		data.parseData(baseURI);
		
		System.out.println(dictionary);
		
		queries.setDictionary(dictionary);
		
		queries.setIndex(index);
		
		queries.setQueryFile(queryFile);
		
		queries.parseQueries(baseURI);
		
		queries.initNbQuesiesWithSameNbPatronsFromOneToTen();
		
		//queries.initParsedQueriesNbRepetitions();
		
		//queries.initNbRepetitionsInQueries();
		
	}

	public static void testForTwoInstances() throws FileNotFoundException, IOException {
		
		queryFile = "data/queries.queryset";
		
	

		dataFile = "data/data-500K.txt";

		init(dataFile,queryFile);
		
		System.out.println("pour une instance de 500K");

		System.out.println("Les requetes avec zero reponse : "+queries.getNbQuesiesWithZeroResponse() );
		
		System.out.println("Les requetes avec un meme nombre de conditions " + queries.getNbQuesiesWithSameNbPatronsFromOneToTen());


		
		System.out.println("pour une instance de 1M");
		
		dataFile = "data/data-1M.txt";

		init(dataFile,queryFile);

		System.out.println("Les requetes avec zero reponse : "+queries.getNbQuesiesWithZeroResponse() );
		
		//System.out.println("Les requetes avec un meme nombre de conditions " + queries.getNbQuesiesWithSameNbPatronsFromOneToTen());

		//System.out.println("Les doublons dans les requêtes " + queries.getNbRepetitionsInQueries());

	
	}
	
	public static void output() {
		
		File file = new File(outputFile);
	}
	
	public static void createOptions(String[] args) {
		
		Options options = new Options();
        
        Option data = Option.builder("data").longOpt("data")
                .argName("data")
                .hasArg()
                .required(true)
                .build();
        options.addOption(data);
        
        Option queries = Option.builder("queries").longOpt("queries")
                .argName("queries")
                .hasArg()
                .required(true)
                .build();
        Option outputFile = Option.builder("output").longOpt("output")
                .argName("output")
                .hasArg()
                .required(true)
                .build();
        options.addOption(data);
        
        options.addOption(queries);
        
       // options.addOption(queries);


        CommandLine cl;
        CommandLineParser clparser = new BasicParser();
        HelpFormatter helper = new HelpFormatter();

        try {
        	
        	cl = clparser.parse(options, args);
            if(cl.hasOption("data")) {
            	dataFile = cl.getOptionValue("data");
            }
          
            if (cl.hasOption("queries")) {
            	queryFile = cl.getOptionValue("queries");
            }
            
            
        } catch (ParseException e) {
        	
            System.out.println(e.getMessage());
            helper.printHelp("Usage:", options);
            System.exit(0);
        }

	}

}
