package rdfQueryEngine.data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.BasicConfigurator;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;

import rdfQueryEngine.dictionary.Dictionary;
import rdfQueryEngine.index.Index;

public class Data {
	
	private String dataFile;
	
	private  Dictionary dictionnary;	
	
	private  Index index;	
	
	//private StopWatch parseDataTime = new StopWatch();
		
	
	public void parseData(String baseURI) throws FileNotFoundException, IOException {
		
		//this.parseDataTime.start();
		
		BasicConfigurator.configure();

		try (Reader dataReader = new FileReader(this.dataFile)) {

			RDFParser rdfParser = Rio.createParser(RDFFormat.NTRIPLES);
			
			MainRDFHandler mainRDFHandler =new MainRDFHandler();
			
			mainRDFHandler.setData(this);
						
			rdfParser.setRDFHandler(mainRDFHandler);
			
			rdfParser.parse(dataReader, baseURI);
			
		}
		
		//this.parseDataTime.stop();
	}

	public String getDataFile() {
		return dataFile;
	}

	public void setDataFile(String dataFile) {
		this.dataFile = dataFile;
	}

	public Dictionary getDictionnary() {
		return dictionnary;
	}

	public void setDictionnary(Dictionary dictionnary) {
		this.dictionnary = dictionnary;
	}

	public Index getIndex() {
		return index;
	}

	public void setIndex(Index index) {
		this.index = index;
	}

	
	

}
