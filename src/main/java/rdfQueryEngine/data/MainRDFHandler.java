package rdfQueryEngine.data;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.helpers.AbstractRDFHandler;

import rdfQueryEngine.dictionary.Dictionary;
import rdfQueryEngine.index.Index;

/**
 * Le RDFHandler intervient lors du parsing de données et permet d'appliquer un traitement pour chaque élément lu par le parseur.
 * 
 * <p>
 * Ce qui servira surtout dans le programme est la méthode {@link #handleStatement(Statement)} qui va permettre de traiter chaque triple lu.
 * </p>
 * <p>
 * À adapter/réécrire selon vos traitements.
 * </p>
 */
public final class MainRDFHandler extends AbstractRDFHandler {
	
	private  Data data;
	
	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public void handleStatement(Statement st) {
		
		
		this.data.getDictionnary().addElement(st);
		

		
		Integer subject = this.data.getDictionnary().getId(st.getSubject().stringValue());
		
		Integer predicat  = this.data.getDictionnary().getId(st.getPredicate().stringValue());
		
		Integer object = this.data.getDictionnary().getId(st.getObject().stringValue());

		this.data.getIndex().addElement(object,predicat,subject);
		


	};
}