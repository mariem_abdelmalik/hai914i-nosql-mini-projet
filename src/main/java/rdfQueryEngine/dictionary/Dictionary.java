package rdfQueryEngine.dictionary;

import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.lang3.time.StopWatch;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.helpers.AbstractRDFHandler;

public class Dictionary {
	
	private Map<Integer,String> d1 ;
	
	private Map<String,Integer> d2 ;
	
	//private StopWatch executionTime;
	
	
	private Integer rang =1;

	public Map<Integer, String> getMap() {
		return d1;
	}

	public void setMap(Map<Integer, String> map) {
		this.d1 = map;
	}
	
	public  Dictionary() {
		
		this.d1 = new HashMap<Integer,String>();
		
		this.d2 = new HashMap<String,Integer>();
		
		//this.executionTime =  new StopWatch();
		
	}
	
	public void addElement(Statement st) {
		
		if (!d2.containsKey(st.getSubject().stringValue())) {
			
			this.d1.put(this.rang, st.getSubject().stringValue());
			
			this.d2.put(st.getSubject().stringValue(),this.rang);

			this.rang++;
		}
		
		if (!d2.containsKey(st.getPredicate().stringValue())) {
			
			this.d1.put(this.rang, st.getPredicate().stringValue());
			
			this.d2.put(st.getPredicate().stringValue(),this.rang);

			rang++;
		}
		
		if (!d2.containsKey(st.getObject().stringValue())) {
			
			this.d1.put(this.rang, st.getObject().stringValue());
			
			this.d2.put(st.getObject().stringValue(),this.rang);
			
			rang++;
			
		}
		
		
	}
	
	
	
	
	
	public Map<Integer, String> getD1() {
		return d1;
	}

	public void setD1(Map<Integer, String> d1) {
		this.d1 = d1;
	}

	public Map<String, Integer> getD2() {
		return d2;
	}

	public void setD2(Map<String, Integer> d2) {
		this.d2 = d2;
	}

	public String getValue(Integer integer) {
		
		return this.d1.get(integer);
		
	}
	
	public Integer getId(String string) {
		
		for (Entry<Integer, String> entry : d1.entrySet()) {
	        if (Objects.equals(string, entry.getValue())) {
	            return entry.getKey();
	        }
	    }
		return -1;
	}
	
	
	public void display() {
		for (Entry<Integer, String> entry : d1.entrySet()) {
			System.out.println(entry.getKey()+" "+entry.getValue());
	       
	    }
	}

	
	

	public Integer getRang() {
		return rang;
	}

	public void setRang(Integer rang) {
		this.rang = rang;
	};
	
	
	

}
