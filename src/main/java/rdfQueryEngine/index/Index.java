package rdfQueryEngine.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.time.StopWatch;
import org.eclipse.rdf4j.model.Statement;

public class Index {
	
	private HashMap<Integer,HashMap<Integer,ArrayList<Integer>>> index;

	//private StopWatch executionTime;

	
	public Index() {
		
		this.index = new HashMap<Integer, HashMap<Integer, ArrayList<Integer>>>();
		
		//this.executionTime = new StopWatch();
	}
	
	public void addElement(Integer first, Integer second, Integer third ) {
			
		if(index.containsKey(first)) {
			
			if(index.get(first).get(second)!=null) {
				
				if (!index.get(first).get(second).contains(third)) {
					
					index.get(first).get(second).add(third);
				}
				
			}				
				
			else {
				
				List<Integer> objects = new ArrayList<Integer>();
				
				objects.add(third);
				
				index.get(first).put(second, (ArrayList<Integer>) objects);
			}
	}
		else {
			
			HashMap<Integer,ArrayList<Integer>> hasmap2 = new HashMap<Integer,ArrayList<Integer>>();
			
			List<Integer> objects = new ArrayList<Integer>();
			
			objects.add(third);
			
			hasmap2.put(second, (ArrayList<Integer>) objects);
			
			index.put(first, hasmap2);
			
		
		}
		
}
	
	public ArrayList<Integer> getElements(Integer first,Integer second){
		
		
		if(index.containsKey(first)) {
			
			if(index.get(first).get(second)!=null) {

				return index.get(first).get(second);
				
			}					

		return new ArrayList<Integer>();
		
	}
		return new ArrayList<Integer>();

	}
			
	public void display() {
		
		for (Entry<Integer, HashMap<Integer,ArrayList<Integer>>> entry : index.entrySet()) {
			System.out.println(" s : "+entry.getKey());
			
			for (Entry<Integer, ArrayList<Integer>> entry2 : entry.getValue().entrySet()) {
				System.out.println(" p : "+entry2.getKey());
				for (Integer i : entry2.getValue()) {
					System.out.println(" o : "+i);
				}
			}
			System.out.println("\n");

		}
		
	};
	
	
}
